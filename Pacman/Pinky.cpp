#include "Pinky.h"
#include "World.h"
#include "Avatar.h"

Pinky::Pinky(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY)
	: Ghost(aPosition, entitySprite, aScatterTargetTileX, aScatterTargetTileY)
{	
}

Pinky::~Pinky(void)
{
}

void Pinky::BehaveChase(World* aWorld, Avatar* avatar)
{	
	std::pair<int, int> targetTileXY = avatar->GetOffsetTileXYBasedOnDirection(4);
	GetPath(aWorld, targetTileXY.first, targetTileXY.second);
}