#include "Pacman.h"
#include "Drawer.h"
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include "SDL.h"
#else
#include "SDL2/SDL.h"
#endif
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "Avatar.h"
#include "World.h"
#include "Ghost.h"
#include "Cherry.h"
#include "Blinky.h"
#include "Pinky.h"
#include "Inky.h"
#include "Clyde.h"
#include "SpriteFont.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
static const std::string fontsPath="freefont-ttf\\sfd\\";
#else
static const std::string fontsPath="freefont-ttf/sfd/";
#endif

Pacman* Pacman::Create(Drawer* aDrawer)
{
	Pacman* pacman = new Pacman(aDrawer);

	if (!pacman->Init())
	{
		delete pacman;
		pacman = NULL;
	}

	return pacman;
}

Pacman::Pacman(Drawer* aDrawer)
: myDrawer(aDrawer)
, myTimeToNextUpdate(0.f)
, myNextMovement(-1.f,0.f)
, myScore(0)
, myFps(0)
, myLives(4)
{
	Sprite* newSprite = NULL;
	std::list<std::string> assetPaths;

	myWorld = new World();

	ghostSpawnTileX = myWorld->GetTileXMax() / 2;
	ghostSpawnTileY = 13;

	avatarSpawnTileX = ghostSpawnTileX;
	avatarSpawnTileY = 22;

	bonusSpawnTileX = ghostSpawnTileX;
	bonusSpawnTileY = ghostSpawnTileY + 3;

	assetPaths.clear();
	assetPaths.push_back("closed_up_32.png");
	assetPaths.push_back("open_up_32.png");
	assetPaths.push_back("closed_down_32.png");
	assetPaths.push_back("open_down_32.png");
	assetPaths.push_back("closed_left_32.png");
	assetPaths.push_back("open_left_32.png");
	assetPaths.push_back("closed_right_32.png");
	assetPaths.push_back("open_right_32.png");
	newSprite = Sprite::Create(assetPaths, myDrawer, 32, 32);
	myAvatar = new Avatar(Vector2f(avatarSpawnTileX*22,avatarSpawnTileY*22), newSprite);

	assetPaths.clear();
	assetPaths.push_back("ghost_32_red.png");
	assetPaths.push_back("Ghost_Vulnerable_32.png");
	assetPaths.push_back("Ghost_Dead_32.png");
	newSprite = Sprite::Create(assetPaths, myDrawer, 32, 32);
	Blinky* blinky = new Blinky(Vector2f(ghostSpawnTileX*22,ghostSpawnTileY*22), newSprite, myWorld->GetTileXMax() - 1, 0);
	ghosts.push_back(blinky);

	assetPaths.clear(); 
	assetPaths.push_back("ghost_32_pink.png");
	assetPaths.push_back("Ghost_Vulnerable_32.png");
	assetPaths.push_back("Ghost_Dead_32.png");
	newSprite = Sprite::Create(assetPaths, myDrawer, 32, 32);
	ghosts.push_back(new Pinky(Vector2f(ghostSpawnTileX*22,ghostSpawnTileY*22), newSprite, 0, 0));

	assetPaths.clear();
	assetPaths.push_back("ghost_32_cyan.png");
	assetPaths.push_back("Ghost_Vulnerable_32.png");
	assetPaths.push_back("Ghost_Dead_32.png");
	newSprite = Sprite::Create(assetPaths, myDrawer, 32, 32);
	ghosts.push_back(new Inky(Vector2f(ghostSpawnTileX*22,ghostSpawnTileY*22), newSprite, myWorld->GetTileXMax() - 1, myWorld->GetTileYMax() - 1, blinky));

	assetPaths.clear();
	assetPaths.push_back("ghost_32_orange.png");
	assetPaths.push_back("Ghost_Vulnerable_32.png");
	assetPaths.push_back("Ghost_Dead_32.png");
	newSprite = Sprite::Create(assetPaths, myDrawer, 32, 32);
	ghosts.push_back(new Clyde(Vector2f(ghostSpawnTileX*22,ghostSpawnTileY*22), newSprite, 0, myWorld->GetTileYMax() - 1));

	gameplayMessage = SpriteFont::Create(fontsPath+"FreeMono.ttf", "", { 255,255,255,255 }, 24, myDrawer);
	scoreDisplay = SpriteFont::Create(fontsPath+"FreeMono.ttf", "", { 0,255,0,255 }, 24, myDrawer);
	livesDisplay = SpriteFont::Create(fontsPath+"FreeMono.ttf", "", { 255,255,255,255 }, 24, myDrawer);
	fpsDisplay = SpriteFont::Create(fontsPath+"FreeMono.ttf", "", { 255,255,255,255 }, 24, myDrawer);

	UpdateLives(myLives);
	UpdateScore(0);
}

Pacman::~Pacman(void)
{
}

bool Pacman::Init()
{
	myWorld->Init(myDrawer);

	return true;
}

bool Pacman::Update(float aTime)
{
	if (!UpdateInput())
		return false;

	if (CheckEndGameCondition())
		return true;
	else if (myLives <= 0)
		return true;

	std::list<Ghost*>::iterator ghostIterator;

	MoveAvatar();
	myAvatar->Update(aTime, myWorld);
	for (ghostIterator = ghosts.begin(); ghostIterator != ghosts.end(); ghostIterator++)
		(*ghostIterator)->Update(aTime, myWorld, myAvatar);

	if(bonus != NULL) 
	{
		if ((bonus->GetPosition() - myAvatar->GetPosition()).Length() < 5.f)
		{
			UpdateScore(100);
			delete bonus;
			bonus = NULL;
		} 
		else 
		{
			bonusTimeRemaining -= aTime;
			if(bonusTimeRemaining < 0) 
			{				
				delete bonus;
				bonus = NULL;
			}
		}
	}
	
	if (myWorld->HasIntersectedDot(myAvatar->GetPosition()))
	{
		myDotsCleared++;
		UpdateScore(10);
		if(CheckEndGameCondition())
			gameplayMessage->SetText("You win!");
		else if(nextBonusIndex < (sizeof(bonusSpawnAfterDotsCleared) / sizeof(int)) 
				&& myDotsCleared >= bonusSpawnAfterDotsCleared[nextBonusIndex])
		{			
			std::list<std::string> assetPaths;
			assetPaths.push_back("cherry.png");
			Sprite* newSprite = Sprite::Create(assetPaths, myDrawer, 32, 32);
			bonus = new Cherry(Vector2f(bonusSpawnTileX * 22, bonusSpawnTileY * 22), newSprite);
			nextBonusIndex++;
			bonusTimeRemaining = bonusTime;
		}

	}

	if (myWorld->HasIntersectedBigDot(myAvatar->GetPosition()))
	{
		UpdateScore(20);
		for (ghostIterator = ghosts.begin(); ghostIterator != ghosts.end(); ghostIterator++)
			(*ghostIterator)->SetFrightened();
	}

	for (ghostIterator = ghosts.begin(); ghostIterator != ghosts.end(); ghostIterator++)
	{
		if (((*ghostIterator)->GetPosition() - myAvatar->GetPosition()).Length() < 16.f)
		{
			if (!(*ghostIterator)->myIsDeadFlag) {
				if ((*ghostIterator)->IsClaimable())
				{
					UpdateScore(50);
					(*ghostIterator)->Die(myWorld);
				}
				else 
				{
					UpdateLives(myLives -1);

					if (myLives > 0)
					{
						myAvatar->Respawn(Vector2f(avatarSpawnTileX * 22, avatarSpawnTileY * 22));
						(*ghostIterator)->Respawn(Vector2f(ghostSpawnTileX * 22, ghostSpawnTileY * 22));
						break;
					}
					else
					{
						gameplayMessage->SetText("You lose!");
						break;
					}
				}
			}
		}
	}
	
	if (aTime > 0)
		SetFPS((int) (1 / aTime));

	return true;
}

void Pacman::UpdateScore(int scoreGain)
{
	myScore += scoreGain;
	std::stringstream stream;
	stream << "Score: ";
	stream << myScore;
	scoreDisplay->SetText(stream.str());
}

void Pacman::UpdateLives(int lives)
{
	myLives = lives;
	std::stringstream stream;
	stream << "Lives: ";
	stream << myLives;
	livesDisplay->SetText(stream.str());
}

void Pacman::SetFPS(int fps)
{
	myFps = fps;
	std::stringstream stream;
	stream << "FPS: ";
	stream << myFps;
	fpsDisplay->SetText(stream.str());
}

bool Pacman::UpdateInput()
{
	const Uint8 *keystate = SDL_GetKeyboardState(NULL);

	if (keystate[SDL_SCANCODE_UP])
		myNextMovement = Vector2f(0.f, -1.f);
	else if (keystate[SDL_SCANCODE_DOWN])
		myNextMovement = Vector2f(0.f, 1.f);
	else if (keystate[SDL_SCANCODE_RIGHT])
		myNextMovement = Vector2f(1.f, 0.f);
	else if (keystate[SDL_SCANCODE_LEFT])
		myNextMovement = Vector2f(-1.f, 0.f);

	if (keystate[SDL_SCANCODE_ESCAPE])
		return false;

	return true;
}

void Pacman::MoveAvatar()
{
	if (myAvatar->IsAtDestination())
	{		
		int nextTileX = myAvatar->GetCurrentTileX() + myNextMovement.myX;
		int nextTileY = myAvatar->GetCurrentTileY() + myNextMovement.myY;

		if (myWorld->TileIsValid(nextTileX, nextTileY))
		{
			myAvatar->SetNextTile(nextTileX, nextTileY);
		}
	}
}

bool Pacman::CheckEndGameCondition()
{
	return myWorld->GetDotCount() == 0;
}

bool Pacman::Draw()
{
	std::list<Ghost*>::iterator ghostIterator;
	myWorld->Draw(myDrawer);
	
	myAvatar->Draw(myDrawer);
	for (ghostIterator = ghosts.begin(); ghostIterator != ghosts.end(); ghostIterator++)
		(*ghostIterator)->Draw(myDrawer);

	myWorld->DrawPortals(myDrawer);

	if(bonus != NULL) 
		bonus->Draw(myDrawer);

	scoreDisplay->Draw(myDrawer, 20, 50);
	livesDisplay->Draw(myDrawer, 20, 80);
	fpsDisplay->Draw(myDrawer, 880, 50);

	if (CheckEndGameCondition() || myLives <= 0)
		gameplayMessage->Draw(myDrawer, 500, 100);
	
	return true;
}
