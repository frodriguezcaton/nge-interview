#ifndef PACMAN_H
#define PACMAN_H

#include "Vector2f.h"
#include <list>

struct SDL_Surface;
class Drawer;
class Avatar;
class World;
class Ghost;
class Blinky;
class Pinky;
class SpriteFont;
class Cherry;

class Pacman
{
public:
	static Pacman* Create(Drawer* aDrawer);
	~Pacman(void);

	bool Update(float aTime);
	bool Draw();

private:
	Pacman(Drawer* aDrawer);
	bool Init();
	bool UpdateInput();
	void MoveAvatar();
	bool CheckEndGameCondition();

	void UpdateScore(int scoreGain);
	void UpdateLives(int lives);
	void SetFPS(int fps);

	Drawer* myDrawer;

	float myTimeToNextUpdate;	

	int ghostSpawnTileX;
	int ghostSpawnTileY;
	int avatarSpawnTileX;
	int avatarSpawnTileY;
	int bonusSpawnTileX;
	int bonusSpawnTileY;

	int myLives;
	int myScore;
	int myDotsCleared = 0;
	int myFps;

	Vector2f myNextMovement;

	Avatar* myAvatar;
	std::list<Ghost*> ghosts;
	World* myWorld;
	Cherry* bonus = NULL;

	SpriteFont* gameplayMessage;
	SpriteFont* scoreDisplay;
	SpriteFont* livesDisplay;
	SpriteFont* fpsDisplay;

	int nextBonusIndex = 0;
	int bonusSpawnAfterDotsCleared[2] = {70, 170};
	int bonusTime = 9;
	float bonusTimeRemaining;
};

#endif // PACMAN_H