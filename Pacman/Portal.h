#ifndef PORTAL_H
#define PORTAL_H

#include "StaticGameEntity.h"
#include "Vector2f.h"

class Portal : public StaticGameEntity
{
public:
	Portal(Vector2f aPosition, Sprite* entitySprite);
	~Portal(void);

	int GetTileX() const { return myPosition.myX / 22; }
	int GetTileY() const { return myPosition.myY / 22; }
};

#endif // PORTAL_H