#ifndef AVATAR_H
#define AVATAR_H

#include "World.h"
#include "MovableGameEntity.h"
#include "Vector2f.h"

#define FRAME_TIME 0.05f

class Avatar : public MovableGameEntity
{
public:
	Avatar(const Vector2f& aPosition, Sprite* entitySprite);
	~Avatar(void);

	void Update(float aTime, World* aWorld);
	void SetNextTile(int anX, int anY) override;
	
	MovementDirection GetMovementDirection() const { return myCurrentDirection; }
	std::pair<int, int> GetOffsetTileXYBasedOnDirection(int tileOffset);

private:

	float myCurrentFrameRemainingTime = FRAME_TIME * 2;
	MovementDirection myCurrentDirection = Left;

	void UpdateAnimation();
};

#endif //AVATAR_H