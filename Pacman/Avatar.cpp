#include "Avatar.h"
#include "Portal.h"

Avatar::Avatar(const Vector2f& aPosition, Sprite* entitySprite)
: MovableGameEntity(aPosition, entitySprite)
{
	sprite->SetFrame(0);
}

Avatar::~Avatar(void)
{
}

void Avatar::Update(float aTime, World* aWorld)
{
	int tileSize = 22;

	Vector2f destination(myNextTileX * tileSize, myNextTileY * tileSize);
	Vector2f direction = destination - myPosition;

	float distanceToMove = aTime * 100.0f;	

	if (distanceToMove > direction.Length())
	{		
		//Check if entered into a portal
		Portal* portal = aWorld->GetPortalTile(myNextTileX, myNextTileY);
		if(portal != NULL) 
		{
			Portal* otherPortal = aWorld->GetOtherPortalTile(portal);
			destination.myX += (otherPortal->GetTileX() - portal->GetTileX()) * tileSize;
			myNextTileX = otherPortal->GetTileX();
			myNextTileY = otherPortal->GetTileY();
		}
		
		myPosition = destination;
		myCurrentTileX = myNextTileX;
		myCurrentTileY = myNextTileY;
	}
	else
	{
		direction.Normalize();
		myPosition += direction * distanceToMove;
	}

	myCurrentFrameRemainingTime -= aTime;
	if(myCurrentFrameRemainingTime < 0) 
	{
		float framesCycles = myCurrentFrameRemainingTime / (FRAME_TIME * 2);
		float remainder = myCurrentFrameRemainingTime - myCurrentFrameRemainingTime * (int) framesCycles;
		myCurrentFrameRemainingTime = FRAME_TIME * 2 - remainder;
		UpdateAnimation();
	}
	else if(myCurrentFrameRemainingTime + aTime > FRAME_TIME && myCurrentFrameRemainingTime < FRAME_TIME) 
	{
		UpdateAnimation();
	}
}

void Avatar::SetNextTile(int anX, int anY)
{
	MovableGameEntity::SetNextTile(anX, anY);

	if(anX == myCurrentTileX && anY < myCurrentTileY) 
	{
		myCurrentDirection = Up;
	} 
	else if(anX == myCurrentTileX && anY > myCurrentTileY) 
	{
		myCurrentDirection = Down;
	} 
	else if(anX < myCurrentTileX && anY == myCurrentTileY) 
	{
		myCurrentDirection = Left;
	} 
	else if(anX > myCurrentTileX && anY == myCurrentTileY) 
	{
		myCurrentDirection = Right;
	}	

	UpdateAnimation();
}

void Avatar::UpdateAnimation() 
{	
	int frameAnimationOffset = (int) (1.5f - myCurrentFrameRemainingTime / (FRAME_TIME * 2));
	sprite->SetFrame(myCurrentDirection * 2 + frameAnimationOffset);
}

std::pair<int, int> Avatar::GetOffsetTileXYBasedOnDirection(int tileOffset)
{
	int offsetTileX = myCurrentTileX;
	int offsetTileY = myCurrentTileY;

	switch (myCurrentDirection)
	{
	case Up:
		offsetTileY = (offsetTileY - tileOffset);
		break;
	case Down:
		offsetTileY = (offsetTileY + tileOffset);
		break;
	case Left:
		offsetTileX = (offsetTileX - tileOffset);
		break;
	case Right:
		offsetTileX = (offsetTileX + tileOffset);
		break;
	}

	return std::make_pair(offsetTileX, offsetTileY);
}