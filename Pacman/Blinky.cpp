#include "Blinky.h"
#include "World.h"
#include "Avatar.h"

Blinky::Blinky(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY)
	: Ghost(aPosition, entitySprite, aScatterTargetTileX, aScatterTargetTileY)
{	
}

Blinky::~Blinky(void)
{
}

void Blinky::BehaveChase(World* aWorld, Avatar* avatar)
{	
	GetPath(aWorld, avatar->GetCurrentTileX(), avatar->GetCurrentTileY());	
}