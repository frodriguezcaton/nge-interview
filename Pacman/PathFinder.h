#ifndef PATHFINDER_H
#define PATHFINDER_H

#include <iostream>
#include <vector>
#include <array>
#include "World.h"

#define X_MAX 28
#define X_STEP 1
#define Y_MAX 29
#define Y_STEP 1

class World;

struct Node
{
    int y;
    int x;
    int parentX;
    int parentY;
    float gCost;
    float hCost; 
    float fCost;
};

inline bool operator < (const Node& lhs, const Node& rhs)
{//We need to overload "<" to put our struct into a set
    return lhs.fCost < rhs.fCost;
}

class PathFinder
{
public:
	PathFinder(World* world);
	~PathFinder(void);
	
	bool IsValid(int x, int y); //If our Node is an obstacle it is not valid

	static double CalculateH(int x, int y, Node dest) {
		double H = (sqrt((x - dest.x)*(x - dest.x)
			+ (y - dest.y)*(y - dest.y)));
		return H;
	}

	int GetXMax() const { return X_MAX; }
	int GetYMax() const { return Y_MAX; }

	std::vector<Node> FindPath(int aFromX, int aFromY, int aToX, int aToY);
	
private:

	World* myWorld;

	std::vector<Node> AStar(Node origin, Node destination);
	std::vector<Node> MakePath(std::array<std::array<Node, (Y_MAX / Y_STEP)>, (X_MAX / X_STEP)> map, Node destination);

	int neighbors[4][2] = {{0,-1}, {1,0}, {0,1}, {-1,0}};
};

#endif // PATHFINDER_H