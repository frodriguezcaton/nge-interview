#include "Inky.h"
#include "World.h"
#include "Avatar.h"

Inky::Inky(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY, Blinky* blinky)
	: Ghost(aPosition, entitySprite, aScatterTargetTileX, aScatterTargetTileY),
	blinky(blinky)
{	
}

Inky::~Inky(void)
{
}

void Inky::BehaveChase(World* aWorld, Avatar* avatar)
{		
	std::pair<int, int> avatarOffsetTileXY = avatar->GetOffsetTileXYBasedOnDirection(2);

	int targetTileX = avatarOffsetTileXY.first  * 2 - blinky->GetCurrentTileX();
	int targetTileY = avatarOffsetTileXY.second * 2 - blinky->GetCurrentTileY();

	GetPath(aWorld, targetTileX, targetTileY);
}