#ifndef GAMEENTITY_H
#define GAMEENTITY_H

#include "Vector2f.h"
#include "Sprite.h"

class Drawer;

class GameEntity
{
public:
	GameEntity(const Vector2f& aPosition, Sprite* entitySprite);
	~GameEntity(void);

	Vector2f GetPosition() const { return myPosition; }
	void SetPosition(const Vector2f& aPosition){ myPosition = aPosition; }

	virtual void Draw(Drawer* aDrawer);

protected:

	Sprite* sprite;
	Vector2f myPosition;
};

#endif // GAMEENTITY_H