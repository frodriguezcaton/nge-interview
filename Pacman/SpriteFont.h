#ifndef SPRITE_FONT_H
#define SPRITE_FONT_H

#include <list>
#include <map>
#include <iostream>
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#else
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"
#endif

struct SDL_Texture;
class Drawer;

class SpriteFont
{
public:
	static SpriteFont* Create(std::string assetPath, std::string initialText, SDL_Color color, int size, Drawer* drawer);
	~SpriteFont(void);

	void SetText(std::string newTextString);
	void SetColor(SDL_Color newColor);
	void Draw(Drawer* drawer, int posX, int posY);

private:
	SpriteFont(TTF_Font* font, std::string initialText, SDL_Color initialColor, Drawer* drawer);
	void PrintToTexture();

	std::string text;
	TTF_Font* fontResource;
	SDL_Texture* printedText;
	SDL_Color fontColor;
	SDL_Rect frame;
	Drawer* drawer;
};

#endif // SPRITE_FONT_H