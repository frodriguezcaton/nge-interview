#include "Ghost.h"
#include "World.h"
#include "PathmapTile.h"
#include "Portal.h"
#include "Drawer.h"
#include "Avatar.h"
#include "Portal.h"

constexpr int Ghost::scatterPeriods[];
constexpr int Ghost::chasePeriods[];

Ghost::Ghost(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY)
	: MovableGameEntity(aPosition, entitySprite),
	myScatterTargetTileX(aScatterTargetTileX),
	myScatterTargetTileY(aScatterTargetTileY)
{
	sprite->SetFrame(0);

	myIsDeadFlag = false;
	
	myPreviousTileX = -1;
	myPreviousTileY = -1;

	//Every ghost starts in scatter mode
	myBehavior = Scatter;
	myRemainingTimeToSwitchScatterChaseBehaviors = scatterPeriods[0];
}

Ghost::~Ghost(void)
{
}

void Ghost::Die(World* aWorld)
{	
	sprite->SetFrame(2);
	myIsDeadFlag = true;
	myPath.clear();
	aWorld->GetPath(myCurrentTileX, myCurrentTileY, 13, 13, myPath);
}

void Ghost::SetBehavior(GhostBehavior aBehavior) 
{ 	
	if(myBehavior == aBehavior)
	{
		return;
	}

	//Ghosts are forced to reverse direction by the system anytime the mode changes from:
	//			- chase-to-scatter
    //			- chase-to-frightened
	//			- scatter-to-chase
	//			- scatter-to-frightened.
	//Ghosts do not reverse direction when changing back from frightened to chase or scatter modes. 
	reverseDirection = myBehavior != Frightened;

	std::cout << "Changed Behavior from ";
	PrintBehaviorToCout();
	std::cout << " to ";
	myBehavior = aBehavior; 
	PrintBehaviorToCout();		
	std::cout << '\n';	
}

void Ghost::SetFrightened() 
{
	if(myBehavior != Frightened) 
	{
		myPath.clear();
		sprite->SetFrame(1);
		myWasInScatterBeforeFrightened = myBehavior == Scatter;
		SetBehavior(Frightened);
		myRemainingTimeInFrightenedBehavior = frightenedTime;
	} 
}

void Ghost::Update(float aTime, World* aWorld, Avatar* avatar)
{
	float speed;
	if (myIsDeadFlag) 
	{
		speed = 200.f;
	} 
	else
	{
		if(myBehavior == Frightened) 
		{
			speed = 40.f;
			myRemainingTimeInFrightenedBehavior -= aTime;
			if(myRemainingTimeInFrightenedBehavior < 0) 
			{
				EndFrightenedBehavior();
			}
			else if(myRemainingTimeInFrightenedBehavior < frightenedFlashingTime) //Flashing (Frightened time about to end)
			{
				float flashingTimeElapsed = frightenedFlashingTime - myRemainingTimeInFrightenedBehavior;	
				sprite->SetFrame(1 - ((int) (flashingTimeElapsed / FLASH_INTERVAL)) % 2);			
			}
		}
		else 
		{
			speed = 60.f;
			CheckScatterChaseBehaviorSwitching(aTime);
		}
	}

	if (IsAtDestination())
	{
		if(myIsDeadFlag) 
		{	
			if (myPath.empty())
			{
				myPreviousTileX = -1;
				myPreviousTileY = -1;
				myIsDeadFlag = false;
				EndFrightenedBehavior();
				Behave(aWorld, avatar);				
			}			
		} 
		else 
		{
			if(reverseDirection) 
			{
				//Check if came through portal
				Portal* portal = aWorld->GetPortalTile(myCurrentTileX, myCurrentTileY);
				if(portal != NULL) 
				{
					Portal* otherPortal = aWorld->GetOtherPortalTile(portal);
					myPosition.myX += (otherPortal->GetTileX() - portal->GetTileX()) * 22;

					myCurrentTileX = otherPortal->GetTileX();
					myCurrentTileY = otherPortal->GetTileY();
				}

				//Go to previous tile
				myPath.clear();
				myPath.push_back(aWorld->GetTile(myPreviousTileX, myPreviousTileY));
				reverseDirection = false;
			}
			else 
			{
				PathmapTile* currentTile = aWorld->GetTile(myCurrentTileX, myCurrentTileY);

				//If in path to portal (can't reverse if not forced) continue to the portal
				if(currentTile->myPathToPortalFlag != 0 && myPreviousTileX - currentTile->myPathToPortalFlag == myCurrentTileX) 
				{
					int nextPathToPortalTileX = myCurrentTileX + currentTile->myPathToPortalFlag; 
					PathmapTile* nextPathToPortalTile;
					do
					{
						nextPathToPortalTile = aWorld->GetTile(nextPathToPortalTileX, myCurrentTileY);
						myPath.push_back(nextPathToPortalTile);
						nextPathToPortalTileX += currentTile->myPathToPortalFlag;
					} while (nextPathToPortalTile->myPathToPortalFlag == currentTile->myPathToPortalFlag);
					//Finally add the portal itself
					myPath.push_back(nextPathToPortalTile);
				}
				else if(currentTile->myIsInstersectionFlag || myPath.empty()) 
				{
					Behave(aWorld, avatar);
				}		
			}
		}

		PathmapTile* nextTile = myPath.front();
		myPath.pop_front();
		SetNextTile(nextTile->myX, nextTile->myY);
	}

	int tileSize = 22;
	Vector2f destination(myNextTileX * tileSize, myNextTileY * tileSize);
	Vector2f direction = destination - myPosition;

	float distanceToMove = aTime * speed;

	if (distanceToMove > direction.Length())
	{
		//Check if entered into a portal
		Portal* portal = aWorld->GetPortalTile(myNextTileX, myNextTileY);
		if(portal != NULL) 
		{
			Portal* otherPortal = aWorld->GetOtherPortalTile(portal);
			destination.myX += (otherPortal->GetTileX() - portal->GetTileX()) * tileSize;
			myNextTileX = otherPortal->GetTileX();
			myNextTileY = otherPortal->GetTileY();
		}
		
		myPosition = destination;
		myPreviousTileX = myCurrentTileX;
		myPreviousTileY = myCurrentTileY;
		myCurrentTileX = myNextTileX;
		myCurrentTileY = myNextTileY;
	}
	else
	{
		direction.Normalize();
		myPosition += direction * distanceToMove;
	}
}

void Ghost::EndFrightenedBehavior() {
	myBehavior = myWasInScatterBeforeFrightened ? Scatter : Chase;	
	sprite->SetFrame(0);			
	CheckScatterChaseBehaviorSwitching(-myRemainingTimeInFrightenedBehavior);
}

void Ghost::Behave(World* aWorld, Avatar* avatar) 
{
	switch (myBehavior)
	{
	case Chase:
		BehaveChase(aWorld, avatar);
		break;
	case Scatter:
		BehaveScatter(aWorld);
		break;
	case Frightened:
		BehaveFrightened(aWorld);
		break;
	}
}
 
void Ghost::CheckScatterChaseBehaviorSwitching(float aTime) 
{
	if (chasePeriods[myChasePeriodIndex] >= 0) //Scatter and chase mode time are paused while FRIGHTENED	
	{
		myRemainingTimeToSwitchScatterChaseBehaviors -= aTime;

		//Change between Chase and Scatter modes
		if(myRemainingTimeToSwitchScatterChaseBehaviors < 0) 
		{				
			switch (myBehavior)
			{
			case Chase:
				SetBehavior(Scatter);
				myScatterPeriodIndex++;
				myRemainingTimeToSwitchScatterChaseBehaviors = scatterPeriods[myScatterPeriodIndex] + myRemainingTimeToSwitchScatterChaseBehaviors;
				break;
			case Scatter:
				SetBehavior(Chase);
				myChasePeriodIndex++;
				myRemainingTimeToSwitchScatterChaseBehaviors = chasePeriods[myChasePeriodIndex] + myRemainingTimeToSwitchScatterChaseBehaviors;
				break;
			}
		}
	}
}

void Ghost::BehaveChase(World* aWorld, Avatar* avatar)
{	
	GetPath(aWorld, avatar->GetCurrentTileX(), avatar->GetCurrentTileY());	
}

void Ghost::BehaveScatter(World* aWorld)
{
	int nextScatterTargetTileX;
	int nextScatterTargetTileY;
	if(myCurrentTileX == myScatterTargetTileX && myCurrentTileY == myScatterTargetTileY) 
	{
		PathmapTile* closestIntersectionTile = aWorld->GetClosestIntersectionTile(myScatterTargetTileX, myScatterTargetTileY);
		nextScatterTargetTileX = closestIntersectionTile->myX;
		nextScatterTargetTileY = closestIntersectionTile->myY;
	} 
	else 
	{
		nextScatterTargetTileX = myScatterTargetTileX;
		nextScatterTargetTileY = myScatterTargetTileY;
	}
	GetPath(aWorld, nextScatterTargetTileX, nextScatterTargetTileY);
}

void Ghost::BehaveFrightened(World* aWorld)
{		
	std::vector<PathmapTile*> neighbors;
	
	if(myCurrentTileX != myPreviousTileX || myCurrentTileY - 1 != myPreviousTileY) 
	{
		PathmapTile* up = aWorld->GetTile(myCurrentTileX, myCurrentTileY - 1);
		if (up && !up->myIsBlockingFlag) 
			neighbors.push_back(up);
	}

	if(myCurrentTileX != myPreviousTileX || myCurrentTileY + 1 != myPreviousTileY) 
	{
		PathmapTile* down = aWorld->GetTile(myCurrentTileX, myCurrentTileY + 1);
		if (down && !down->myIsBlockingFlag)
			neighbors.push_back(down);
	}

	if(myCurrentTileX + 1 != myPreviousTileX || myCurrentTileY != myPreviousTileY) 
	{
		PathmapTile* right = aWorld->GetTile(myCurrentTileX + 1, myCurrentTileY);
		if (right && !right->myIsBlockingFlag)
			neighbors.push_back(right);
	}

	if(myCurrentTileX - 1 != myPreviousTileX || myCurrentTileY != myPreviousTileY) 
	{
		PathmapTile* left = aWorld->GetTile(myCurrentTileX - 1, myCurrentTileY);
		if (left && !left->myIsBlockingFlag)
			neighbors.push_back(left);
	}

	myPath.push_back(neighbors[rand() % neighbors.size()]);
}

void Ghost::GetPath(World* aWorld, int aToX, int aToY)
{	
	myPath.clear();

	//Added this to prevent reverse direction
	bool isNotInCenter = myPreviousTileX != 13 && myPreviousTileY != 13;
	PathmapTile* previousTile = NULL;
	if(isNotInCenter) 
	{
		previousTile = aWorld->GetTile(myPreviousTileX, myPreviousTileY);
		if(previousTile != NULL) 
			previousTile->myIsBlockingFlag = true;
	}

	aWorld->GetPath(myCurrentTileX, myCurrentTileY, aToX, aToY, myPath);	
	//we are already on the first tile so we remove it from the path
	myPath.pop_front();

	if(isNotInCenter && previousTile != NULL) 
		previousTile->myIsBlockingFlag = false;
}