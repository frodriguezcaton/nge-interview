#ifndef INKY_H
#define INKY_H

#include "Blinky.h"

class World;
class Avatar;
class Blinky;

class Inky : public Ghost
{
public:
	Inky(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY, Blinky* blinky);
	~Inky(void);

protected:
	void BehaveChase(World* aWorld, Avatar* avatar) override;

	Blinky* blinky;
};

#endif // INKY_H