#include "PathmapTile.h"

PathmapTile::PathmapTile(int anX, int anY, bool aIsBlockingFlag, int aPathToPortalFlag)
: myX(anX)
, myY(anY)
, myIsBlockingFlag(aIsBlockingFlag)
, myPathToPortalFlag(aPathToPortalFlag)
{
}

PathmapTile::~PathmapTile(void)
{
}
