#ifndef BLINKY_H
#define BLINKY_H


#include <list>

#include "Ghost.h"

class World;
class Avatar;

class Blinky : public Ghost
{
public:
	Blinky(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY);
	~Blinky(void);

protected:
	void BehaveChase(World* aWorld, Avatar* avatar) override;
};

#endif // BLINKY_H