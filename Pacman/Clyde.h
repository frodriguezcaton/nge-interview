#ifndef CLYDE_H
#define CLYDE_H

#include "Ghost.h"

class World;
class Avatar;

class Clyde : public Ghost
{
public:
	Clyde(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY);
	~Clyde(void);

protected:
	void BehaveChase(World* aWorld, Avatar* avatar) override;
};

#endif // CLYDE_H