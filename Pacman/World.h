#ifndef WORLD_H
#define WORLD_H

#include <list>
#include <map>
#include "Vector2f.h"
#include "Sprite.h"
#include "PathFinder.h"

class Drawer;
class PathmapTile;
class Dot;
class BigDot;
class Cherry;
class Portal;
class PathFinder;

class World
{
public:
	World(void);
	~World(void);

	void Init(Drawer* gameDrawer);

	void Draw(Drawer* aDrawer);
	void DrawPortals(Drawer* aDrawer);
	bool TileIsValid(int anX, int anY);

	bool HasIntersectedDot(const Vector2f& aPosition);
	bool HasIntersectedBigDot(const Vector2f& aPosition);
	int GetDotCount();

	int GetTileXMax();
	int GetTileYMax();
	
	void Update();

	void GetPath(int aFromX, int aFromY, int aToX, int aToY, std::list<PathmapTile*>& path);
	PathmapTile* GetTile(int aFromX, int aFromY);
	Portal* GetPortalTile(int anX, int anY);
	Portal* GetOtherPortalTile(Portal* aPortal);
	PathmapTile* GetClosestIntersectionTile(int aFromX, int aFromY);

private:
	PathmapTile* GetClosestNonBlockingTile(int anX, int anY, bool inclusive);
	bool ListDoesNotContain(PathmapTile* aFromTile, std::list<PathmapTile*>& aList);

	bool InitPathmap();
	bool InitDots(Drawer* gameDrawer);
	bool InitBigDots(Drawer* gameDrawer);
	bool InitPortals(Drawer* gameDrawer);
	void MarkIntersectionTiles();

	std::list<PathmapTile*> myPathmapTiles;
	std::list<Dot*> myDots;
	std::list<BigDot*> myBigDots;
	std::list<Portal*> myPortals;

	Sprite* boardBackground;

	PathFinder* myPathFinder;
};

#endif // WORLD_H