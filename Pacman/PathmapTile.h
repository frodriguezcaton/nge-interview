#ifndef PATHMAPTILE_H
#define PATHMAPTILE_H

#include <cmath>

class PathmapTile
{
public:
	PathmapTile(int anX, int anY, bool aIsBlockingFlag, int aPathToPortalFlag);
	~PathmapTile(void);

	int myX;
	int myY;
	bool myIsBlockingFlag;
	bool myIsInstersectionFlag = false;
	int myPathToPortalFlag;

	int GetDistance(int anX, int anY) const {
		return std::abs(anX - myX) + std::abs(anY - myY);
	}

};

#endif // PATHMAPTILE_H