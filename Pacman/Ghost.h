#ifndef GHOST_H
#define GHOST_H

#include <list>

#include "MovableGameEntity.h"

#define FLASH_INTERVAL 0.25f

class World;
class PathmapTile;
class Avatar;

class Ghost : public MovableGameEntity
{
public:
	enum GhostBehavior
	{
		Chase,
		Scatter,
		Frightened,
	};

	Ghost(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY);
	~Ghost(void);

	void Update(float aTime, World* aWorld, Avatar* avatar);
	
	bool myIsDeadFlag;

	void Die(World* aWorld);
	void SetFrightened();
	bool IsClaimable() const { return myBehavior == Frightened; }

protected:
	void Behave(World* aWorld, Avatar* avatar);
	virtual void BehaveChase(World* aWorld, Avatar* avatar);
	void BehaveFrightened(World* aWorld);
	void BehaveScatter(World* aWorld);
	void CheckScatterChaseBehaviorSwitching(float aFloat);	
	void EndFrightenedBehavior();
	
	void GetPath(World* aWorld, int aToX, int aToY);
	PathmapTile* GetOffsetTileBasedOnDirection(World* aWorld, Avatar* avatar, int tileOffset);
	
	void SetBehavior(GhostBehavior aBehavior);

	void PrintBehaviorToCout() const { 
		switch (myBehavior)
		{
		case Chase:
			std::cout << "Chase";
			break;
		case Scatter:
			std::cout << "Scatter";
			break;
		case Frightened:
			std::cout << "Frightened";
			break;
		}
	}

	GhostBehavior myBehavior;
	bool myWasInScatterBeforeFrightened;
	int myScatterPeriodIndex = 0;
	int myChasePeriodIndex = 0;
	float myRemainingTimeToSwitchScatterChaseBehaviors;
	float myRemainingTimeInFrightenedBehavior;
	bool reverseDirection = false;

	int myScatterTargetTileX;
	int myScatterTargetTileY;

	int myPreviousTileX;
	int myPreviousTileY;

	std::list<PathmapTile*> myPath;
	
	static constexpr const int scatterPeriods[4] = { 7,  7,  5,  5};
	static constexpr const int chasePeriods[4]   = {20, 20, 20, -1};
	const int frightenedTime = 6;
	const float frightenedFlashingTime = 2.5f;
};

#endif // GHOST_H