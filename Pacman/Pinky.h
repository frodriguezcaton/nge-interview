#ifndef PINKY_H
#define PINKY_H

#include "Ghost.h"

class World;
class Avatar;

class Pinky : public Ghost
{
public:
	Pinky(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY);
	~Pinky(void);

protected:
	void BehaveChase(World* aWorld, Avatar* avatar) override;
};

#endif // PINKY_H