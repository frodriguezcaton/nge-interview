#include "World.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "PathmapTile.h"
#include "Dot.h"
#include "BigDot.h"
#include "Portal.h"
#include "Drawer.h"
#include "PathFinder.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
static const std::string mapPath="map.txt";
#else
static const std::string mapPath="data/map.txt";
#endif

World::World(void)
{
}

World::~World(void)
{
}

int World::GetTileXMax() { return myPathFinder->GetXMax(); }
int World::GetTileYMax() { return myPathFinder->GetYMax(); }

void World::Init(Drawer* gameDrawer)
{
	std::list<std::string> assetPaths;
	assetPaths.push_back("playfield.png");
	boardBackground = Sprite::Create(assetPaths, gameDrawer, 1024, 768);
	//boardBackground->SetFrame("playfield.png");

	myPathFinder = new PathFinder(this);

	InitPathmap();
	InitDots(gameDrawer);
	InitBigDots(gameDrawer);
	InitPortals(gameDrawer);

	MarkIntersectionTiles();
}

bool World::InitPathmap()
{
	std::string line;
	std::ifstream myfile (mapPath);
	if (myfile.is_open())
	{
		int lineIndex = 0;
		while (! myfile.eof() )
		{
			std::getline (myfile,line);
			for (unsigned int i = 0; i < line.length(); i++)
			{
				//Check if its a valid character
				if(line[i] == 'x' || line[i] == '.' || line[i] == ' ' || line[i] == 'o' 
				|| line[i] == '=' || line[i] == '<' || line[i] == '>') 
				{
					int pathToPortalFlag;
					if(line[i] == '<') 
						pathToPortalFlag = -1;						
					else if(line[i] == '>') 
						pathToPortalFlag = 1;
					else
						pathToPortalFlag = -0;

					PathmapTile* tile = new PathmapTile(i, lineIndex, line[i] == 'x', pathToPortalFlag);
					myPathmapTiles.push_back(tile);
				}
			}

			lineIndex++;
		}
		myfile.close();
	}

	return true;
}

bool World::InitDots(Drawer* gameDrawer)
{
	Sprite* newSprite = NULL;
	std::list<std::string> assetPaths;

	std::string line;
	std::ifstream myfile (mapPath);

	if (myfile.is_open())
	{
		int lineIndex = 0;
		while (! myfile.eof() )
		{
			std::getline (myfile,line);
			for (unsigned int i = 0; i < line.length(); i++)
			{
				if (line[i] == '.')
				{
					assetPaths.clear();
					assetPaths.push_back("Small_Dot_32.png");
					newSprite = Sprite::Create(assetPaths, gameDrawer, 32, 32);
					Dot* dot = new Dot(Vector2f(i*22, lineIndex*22), newSprite);
					myDots.push_back(dot);
				}
			}

			lineIndex++;
		}
		myfile.close();
	}

	return true;
}

bool World::InitBigDots(Drawer* gameDrawer)
{
	Sprite* newSprite = NULL;
	std::list<std::string> assetPaths;

	std::string line;
	std::ifstream myfile (mapPath);

	if (myfile.is_open())
	{
		int lineIndex = 0;
		while (! myfile.eof() )
		{
			std::getline (myfile,line);
			for (unsigned int i = 0; i < line.length(); i++)
			{
				if (line[i] == 'o')
				{
					assetPaths.clear();
					assetPaths.push_back("Big_Dot_32.png");
					newSprite = Sprite::Create(assetPaths, gameDrawer, 32, 32);
					BigDot* dot = new BigDot(Vector2f(i*22, lineIndex*22), newSprite);
					myBigDots.push_back(dot);
				}
			}

			lineIndex++;
		}
		myfile.close();
	}

	return true;
}

bool World::InitPortals(Drawer* gameDrawer)
{
	Sprite* newSprite = NULL;
	std::list<std::string> assetPaths;

	std::string line;
	std::ifstream myfile (mapPath);

	if (myfile.is_open())
	{
		int lineIndex = 0;
		while (! myfile.eof() )
		{
			std::getline (myfile,line);
			for (unsigned int i = 0; i < line.length(); i++)
			{
				if (line[i] == '=')
				{
					assetPaths.clear();
					assetPaths.push_back("black_box.png");
					newSprite = Sprite::Create(assetPaths, gameDrawer, 32, 32);
					Portal* portal = new Portal(Vector2f(i*22, lineIndex*22), newSprite);
					myPortals.push_back(portal);
				}
			}

			lineIndex++;
		}
		myfile.close();
	}

	return true;
}

void World::MarkIntersectionTiles() 
{
	for(std::list<PathmapTile*>::iterator list_iter = myPathmapTiles.begin(); list_iter != myPathmapTiles.end(); list_iter++)
	{
		PathmapTile* tile = *list_iter;

		if(tile->myIsBlockingFlag) 
		{
			continue;
		}

		int nonBlockingNeighbors = 0;
		if (TileIsValid(tile->myX, tile->myY - 1)) 
		{
			nonBlockingNeighbors++;
		}

		if (TileIsValid(tile->myX, tile->myY + 1)) 
		{
			nonBlockingNeighbors++;
		}

		if (TileIsValid(tile->myX + 1, tile->myY)) 
		{
			if(nonBlockingNeighbors == 2) 
			{
				tile->myIsInstersectionFlag = true;
				continue;
			} 
			else 
			{				
				nonBlockingNeighbors++;
			}
		}

		if (TileIsValid(tile->myX - 1, tile->myY)) 
		{
			tile->myIsInstersectionFlag = nonBlockingNeighbors == 2;
		}
	}
}

void World::Draw(Drawer* aDrawer)
{
	boardBackground->Draw(aDrawer, 22, 0);

	for(std::list<Dot*>::iterator list_iter = myDots.begin(); list_iter != myDots.end(); list_iter++)
	{
		Dot* dot = *list_iter;
		dot->Draw(aDrawer);
	}

	for(std::list<BigDot*>::iterator list_iter = myBigDots.begin(); list_iter != myBigDots.end(); list_iter++)
	{
		BigDot* dot = *list_iter;
		dot->Draw(aDrawer);
	}
}

void World::DrawPortals(Drawer* aDrawer)
{
	for(std::list<Portal*>::iterator list_iter = myPortals.begin(); list_iter != myPortals.end(); list_iter++)
	{
		Portal* portal = *list_iter;
		portal->Draw(aDrawer);
	}
}

bool World::TileIsValid(int anX, int anY)
{
	for(std::list<PathmapTile*>::iterator list_iter = myPathmapTiles.begin(); list_iter != myPathmapTiles.end(); list_iter++)
	{
		PathmapTile* tile = *list_iter;

		if (anX == tile->myX && anY == tile->myY && !tile->myIsBlockingFlag)
			return true;
	}

	return false;
}

int World::GetDotCount()
{
	return myDots.size() + myBigDots.size();
}

bool World::HasIntersectedDot(const Vector2f& aPosition)
{
	for(std::list<Dot*>::iterator list_iter = myDots.begin(); list_iter != myDots.end(); list_iter++)
	{
		Dot* dot = *list_iter;
		if ((dot->GetPosition() - aPosition).Length() < 5.f)
		{
			myDots.remove(dot);
			delete dot;
			return true;
		}
	}

	return false;
}

bool World::HasIntersectedBigDot(const Vector2f& aPosition)
{
	for(std::list<BigDot*>::iterator list_iter = myBigDots.begin(); list_iter != myBigDots.end(); list_iter++)
	{
		BigDot* dot = *list_iter;
		if ((dot->GetPosition() - aPosition).Length() < 5.f)
		{
			myBigDots.remove(dot);
			delete dot;
			return true;
		}
	}

	return false;
}

void World::GetPath(int aFromX, int aFromY, int aToX, int aToY, std::list<PathmapTile*>& path)
{
	PathmapTile* closestNonBlockingTile = GetClosestNonBlockingTile(aToX, aToY, true);
	if(closestNonBlockingTile->myX == aFromX && closestNonBlockingTile->myY == aFromY) {
		closestNonBlockingTile = GetClosestNonBlockingTile(closestNonBlockingTile->myX, closestNonBlockingTile->myY, false);
	}

	for (Node node : myPathFinder->FindPath(aFromX, aFromY, closestNonBlockingTile->myX, closestNonBlockingTile->myY)) {
		path.push_back(GetTile(node.x, node.y));
	}
}

PathmapTile* World::GetTile(int aFromX, int aFromY)
{
	for(std::list<PathmapTile*>::iterator list_iter = myPathmapTiles.begin(); list_iter != myPathmapTiles.end(); list_iter++)
	{
		PathmapTile* tile = *list_iter;
		if (tile->myX == aFromX && tile->myY == aFromY)
		{
			return tile;
		}
	}

	return NULL;
}


Portal* World::GetPortalTile(int anX, int anY) 
{
	for(std::list<Portal*>::iterator list_iter = myPortals.begin(); list_iter != myPortals.end(); list_iter++)
	{
		Portal* portal = *list_iter;
		if (portal->GetTileX() == anX && portal->GetTileY() == anY)
		{
			return portal;
		}
	}
	return NULL;
}

Portal* World::GetOtherPortalTile(Portal* aPortal)
{
	for(std::list<Portal*>::iterator list_iter = myPortals.begin(); list_iter != myPortals.end(); list_iter++)
	{
		if (*list_iter != aPortal)
		{
			return *list_iter;
		}
	}
	return NULL;
}

PathmapTile* World::GetClosestIntersectionTile(int aFromX, int aFromY)
{
	PathmapTile* closestIntersectionTile = NULL;
	int closestIntersectionTileDistance;	
	for(std::list<PathmapTile*>::iterator list_iter = myPathmapTiles.begin(); list_iter != myPathmapTiles.end(); list_iter++)
	{
		PathmapTile* tile = *list_iter;
		if (tile->myX != aFromX && tile->myY != aFromY)
		{
			if(!tile->myIsBlockingFlag && tile->myIsInstersectionFlag) 
			{				
				int intersectionTileDistance = tile->GetDistance(aFromX, aFromY);
				if (closestIntersectionTile == NULL || intersectionTileDistance < closestIntersectionTileDistance) 
				{
					closestIntersectionTile = tile;
					closestIntersectionTileDistance = intersectionTileDistance;
				}
			}
		}
	}

	return closestIntersectionTile;
}

PathmapTile* World::GetClosestNonBlockingTile(int anX, int anY, bool inclusive)
{
	PathmapTile* closestIntersectionTile = NULL;
	int closestIntersectionTileDistance;	
	for(std::list<PathmapTile*>::iterator list_iter = myPathmapTiles.begin(); list_iter != myPathmapTiles.end(); list_iter++)
	{
		PathmapTile* tile = *list_iter;
		if (inclusive || (tile->myX != anX && tile->myY != anY)) {
			if(!tile->myIsBlockingFlag) 
			{				
				int intersectionTileDistance = tile->GetDistance(anX, anY);
				if (closestIntersectionTile == NULL || intersectionTileDistance < closestIntersectionTileDistance) 
				{
					closestIntersectionTile = tile;
					closestIntersectionTileDistance = intersectionTileDistance;
				}
			}
		}
	}

	return closestIntersectionTile;
}

bool World::ListDoesNotContain(PathmapTile* aFromTile, std::list<PathmapTile*>& aList)
{
	for(std::list<PathmapTile*>::iterator list_iter = aList.begin(); list_iter != aList.end(); list_iter++)
	{
		PathmapTile* tile = *list_iter;
		if (tile == aFromTile)
		{
			return false;
		}
	}

	return true;
}

bool SortFromGhostSpawn(PathmapTile* a, PathmapTile* b)
{
	int la = abs(a->myX - X_MAX / 2) + abs(a->myY - 13);
	int lb = abs(b->myX - X_MAX / 2) + abs(b->myY - 13);

    return la < lb;
}