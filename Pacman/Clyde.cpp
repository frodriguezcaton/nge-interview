#include "Clyde.h"
#include "World.h"
#include "Avatar.h"

#include <cmath>

Clyde::Clyde(const Vector2f& aPosition, Sprite* entitySprite, int aScatterTargetTileX, int aScatterTargetTileY)
	: Ghost(aPosition, entitySprite, aScatterTargetTileX, aScatterTargetTileY)
{	
}

Clyde::~Clyde(void)
{
}

void Clyde::BehaveChase(World* aWorld, Avatar* avatar)
{	
	int distanceX = std::abs(avatar->GetCurrentTileX() - myCurrentTileX);
	int distanceY = std::abs(avatar->GetCurrentTileY() - myCurrentTileY);
	int distanceSqr = distanceX * 2 + distanceY * 2;

	if(distanceSqr > 16) 
		GetPath(aWorld, avatar->GetCurrentTileX(), avatar->GetCurrentTileY());
	else 
		BehaveScatter(aWorld);
}