#include "SpriteFont.h"
#include "Drawer.h"
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"
#else
#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_ttf.h"
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
static const std::string dataPath="";
#else
static const std::string dataPath="data/";
#endif

SpriteFont* SpriteFont::Create(std::string assetPath, std::string initialText, SDL_Color initialColor, int size, Drawer* drawer)
{
	TTF_Font* font = drawer->GetFontResource(dataPath + assetPath, size);
	SpriteFont* newFont = new SpriteFont(font, initialText, initialColor, drawer);
	return newFont;
}

SpriteFont::SpriteFont(TTF_Font* font, std::string initialText, SDL_Color initialColor, Drawer* drawer)
	: fontResource(font),
	text(initialText),
	fontColor(initialColor),
	drawer(drawer)
{
	PrintToTexture();
}

SpriteFont::~SpriteFont(void)
{
	SDL_DestroyTexture(printedText);
	TTF_CloseFont(fontResource);
}

void SpriteFont::SetText(std::string newTextString)
{
	text = newTextString;
	PrintToTexture();
}

void SpriteFont::SetColor(SDL_Color newColor)
{
	fontColor = newColor;
	PrintToTexture();
}

void SpriteFont::PrintToTexture()
{
	SDL_Surface* surface = TTF_RenderText_Solid(fontResource, text.c_str(), fontColor);
	if (!surface)
		return;

	printedText = drawer->GetTextureResource(surface);

	frame.x = 0;
	frame.y = 0;
	frame.w = surface->w;
	frame.h = surface->h;

	SDL_FreeSurface(surface);
}

void SpriteFont::Draw(Drawer* drawer, int posX, int posY)
{
	drawer->Draw(printedText, frame, posX, posY);
}