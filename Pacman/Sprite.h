#ifndef SPRITE_H
#define SPRITE_H

#include <list>
#include <map>
#include <iostream>
#include <vector>
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include "SDL_image.h"
#include "SDL_rect.h"
#else
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_rect.h"
#endif

struct SDL_Texture;
class Drawer;

class Sprite
{
public:
	static Sprite* Create(std::list<std::string> assetPaths, Drawer* drawer, int sizeX, int sizeY);
	~Sprite(void);

	void SetFrame(int frameIndex);
	void Draw(Drawer* drawer, int posX, int posY);

private:
	Sprite(std::vector<SDL_Texture*> frameCollection, SDL_Rect sizeRect);
	
	std::vector<SDL_Texture*> textures;

	int currentFrameIndex = 0;
	SDL_Rect frame;
};

#endif // SPRITE_H