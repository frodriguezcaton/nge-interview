#include "Sprite.h"
#include "Drawer.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include "SDL_image.h"
#include "SDL_rect.h"
#include "SDL.h"
#else
#include "SDL2/SDL_image.h"
#include "SDL2/SDL_rect.h"
#include "SDL2/SDL.h"
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
static const std::string dataPath="";
#else
static const std::string dataPath="data/";
#endif

Sprite* Sprite::Create(std::list<std::string> assetPaths, Drawer* drawer, int sizeX, int sizeY)
{
	std::vector<SDL_Texture*> frameCollection;
	std::list<std::string>::iterator it;

	for(it = assetPaths.begin(); it != assetPaths.end(); it++)
	{
		SDL_Texture* texture = drawer->GetTextureResource(dataPath+*it);
		frameCollection.push_back(texture);
	}

	SDL_Rect sizeRect;
    sizeRect.x = 0;
    sizeRect.y = 0;
    sizeRect.w = sizeX;
    sizeRect.h = sizeY;

	Sprite* newSprite = new Sprite(frameCollection, sizeRect);
	return newSprite;
}

void Sprite::SetFrame(int frameIndex)
{
	if(textures.size() > frameIndex)
		currentFrameIndex = frameIndex;
}

Sprite::Sprite(std::vector<SDL_Texture*> frameCollection, SDL_Rect sizeRect)
	: frame(sizeRect)
{
	textures = frameCollection;
}

void Sprite::Draw(Drawer* drawer, int posX, int posY)
{
	drawer->Draw(textures[currentFrameIndex], frame, posX, posY);
}